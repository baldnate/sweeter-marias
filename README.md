sweeterMarias
=============

sweeterMarias is an improved presentation of Sweet Maria's green coffee info.

Structure
---------
The Python script scrapes the Sweet Maria's website and dumps the info in a json file.  The webpage allows interactive access to the coffee information.

Wish list
---------

Allow the display of:

* Mini reviews
* Link to full review

Allow filtering based on:

* roast levels
* origin
* processing
* keywords (fruited, earthy, etc.)
